:: env file to make Windows Command Prompt a little more UNIX-like :)

@echo off

:: Standard Aliases

DOSKEY ls=dir /B
DOSKEY rm=del
DOSKEY cp=copy

:: Git aliases  (For the strange person who wants to not use Git Bash)

DOSKEY g=git
DOSKEY gst=git status
DOSKEY gpm=git push -u origin master
DOSKEY gu=git pull
