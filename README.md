# cmd-config

Run `install.bat` to copy the cmd file onto your system and set it to be
executed at the start of every Command Prompt session.

env.cmd will be copied to the root of your C drive.

It will be set to execute at Command Prompt startup by adding an AutoRun key
at HKEY_CURRENT_USER\Software\Microsoft\Command Processor in your registry

